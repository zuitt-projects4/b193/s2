package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraySample {

    public static void main(String[] args){
        // Arrays = list of things. In javascript, we can have as many as we want but in Java, it is limited.

        //Array Declaration/Instantiation

        int[]  intArray = new int[3];

        //the int[] - indicate that this is an int data type and can hold multiple int values.
        // new keyword tells Java to create the said Identifier
        // int[3] indicates the amount of integer values in the array

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);

        int intSample[] = new int[3];
        intSample[0] = 50;
        System.out.println(intSample[0]);

        //String Array
        String stringArray[] = new String[3];
        stringArray[0] = "Anya";
        stringArray[1] = "Loid";
        stringArray[2] = "Yor";
        System.out.println(stringArray[2]);

        //Declaration with initialization based on number of elements
        int[] intArray2 = {100, 200,300,400, 500};

        System.out.println(intArray2); // it will print out the memory allocation of the array

        //to get the actual values of array
        System.out.println(Arrays.toString(intArray2));

        //Arrays is a class contains various methods for manipulating arrays
        //.toString is a method that returns a string representation of th contents of the specific array.

        //methods used in arrays

        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));

        //binarySearch
        String searchTerm = "Loid";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult); // the binaryResult will return an index if the match is found.
        //if the searchTerm is not available in the array, the output will give a result of -(insertion point). The insertion point is defined as the point at which the key would be inserted into the array.
        // the array must be sorted as by the Arrays.sort() method before using the binarySearch() method.

       //Multidimensional Arrays
       //two-dimensional array - can be described by two nested lengths nested each other, like a matrix

       String[][] classroom = new String[3][3];
       //First Row
        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Chaeyoung";
        classroom[0][2] = "Nayeon";
        //Second Row
        classroom[1][0] = "Luffy";
        classroom[1][1] = "Zorro";
        classroom[1][2] = "Sanji";
        //Third Row
        classroom[2][0] = "Loid";
        classroom[2][1] = "Yor";
        classroom[2][2] = "Anya";

        System.out.println(Arrays.deepToString(classroom));//deeptostring() returns a string of the nested / deep contents of the array.

        System.out.println(Arrays.toString(classroom[0]));
        System.out.println(Arrays.toString(classroom[1]));
        System.out.println(Arrays.toString(classroom[2]));

        //ArrayLists - is a resizable-array. Implements all optional list operations, and permits all elements and can be added or removed  whenever it is needed.

        ArrayList<String> students = new ArrayList<>();

        //adding elements to an ArrayLists is done by using the add()
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        //retrieving elements using .get()
        System.out.println(students.get(0));
        //update or changing elements using .set()
        students.set(1,"George");//first value => index ; second value => updatedValue
        System.out.println(students);
        //delete or remove elements using .remove()
        students.remove(1); // index number
        System.out.println(students);
        //removing ALL elements in the ArrayList
        students.clear();//clear all the lists of elements
        System.out.println(students);
        //grt length of an ArrayList
        System.out.println(students.size());

        //ArrayLists with initial values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));
        System.out.println(employees);

        //HashMaps - are stored by specifying the data type of the key and of the value
        //flexible object without pattern
        //HashMap<fieldDataType, valueDataType>
        HashMap<String, String> employeeRole = new HashMap<>();

        //adding elements to HashMaps by using put()
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");

        System.out.println(employeeRole);

        //retrieve the value using field
        System.out.println(employeeRole.get("Captain"));
        //remove fields
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

        //getting the keys of the elements of HashMap by using keyset()
        System.out.println(employeeRole.keySet());

        //HashMaps with int as values
        HashMap<String , ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradeListA = new ArrayList<>(Arrays.asList(80, 75, 90));
        ArrayList<Integer> gradeListB = new ArrayList<>(Arrays.asList(86, 87, 96));

        subjectGrades.put("Joe", gradeListA);
        subjectGrades.put("John", gradeListB);

        System.out.println(subjectGrades);

        //get a specific value
        System.out.println(subjectGrades.get("Joe").get(2));




    }

}
