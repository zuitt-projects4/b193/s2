package com.zuitt.batch193;

public class ControlStructure {

    public static void main(String[] args){

        //Operators
        //Types:
        //1. Arithmetic => +, - , * , /, %, +=, -=, *=, /=, %=, ++, --
        //2. Comparison => >, < , ==, ===, !=, <=, >=
        //3. Logical => &,|, !, ||, && , ? :
        //4. Assignment => =

        //Control Structures
        int num1 = 10;
        int num2 = 20;

        /*
         if(num1 > 5){
            System.out.println("num1 is greater than 5");
         }
        */

        if(num1 > 5)
            System.out.println("num1 is greater than 5");

        //if-else statement
        if(num2 > 100)
            System.out.println("num2 is greater than 100");
        else
            System.out.println("num2 is less than 100");

        //if-else if -  else statement
        if(num1 == 5)
            System.out.println("num1 is equal to 5");
        else if(num2 == 20)
            System.out.println("num2 is equal to 20");
        else
            System.out.println("Anything else");

        //short-circuiting, it is a technique applicable only to the AND and OR operators wherein

        //& and | operators, when used as logical operators, always evaluates both sides

        //&& and || operators  "short-circuit",
        //false && ... => it is not necessary to know what the right hand side is, because the result can only be false regardless of the value
        //true ||

        int x = 15;
        int y = 0;

        if(y>5 && x/y ==0)
        System.out.println("Result is: " +x/y);
        else
            System.out.println("The condition has short circuited");

        //Switch statements
        //Direction (North, East, West, South)

        int directionValue = 3;

        switch (directionValue){
            case 1: //A case block within a switch represents a single case, or a single possible value for the statement
                System.out.println("North");
                break; // the break keyword tell that this specific case block has finished.
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default://handles scenarios if there are no cases that were satisfied
                System.out.println("Invalid");
        }





    }

}
