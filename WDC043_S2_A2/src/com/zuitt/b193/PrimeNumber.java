package com.zuitt.b193;

import java.util.Arrays;
import java.util.Scanner;

public class PrimeNumber {

    public static void main(String[] args){

        int primeNumbers[]  =new int[5];

        primeNumbers[0] =  2;
        primeNumbers[1] =  3;
        primeNumbers[2] =  5;
        primeNumbers[3] =  7;
        primeNumbers[4] =  11;

       Scanner appScanner = new Scanner(System.in);
        System.out.println("Pick from 1 to 5 index to return Prime Number: ");
        int index = appScanner.nextInt();

        switch(index){
            case 1:
                System.out.println("The first prime number is:  " + primeNumbers[0]);
                break;
            case 2:
                System.out.println("The second prime number is:  " + primeNumbers[1]);
                break;
            case 3:
                System.out.println("The third prime number is:  " + primeNumbers[2]);
                 break;
            case 4:
                System.out.println("The fourth prime number is:  " + primeNumbers[3]);
                break;
            case 5:
                System.out.println("The fifth prime number is:  " + primeNumbers[4]);
                break;
            default:
                System.out.println("Invalid. Just pick from 1 to 5");
        }
    }

}
