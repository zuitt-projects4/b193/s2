package com.zuitt.b193;

import java.util.ArrayList;

public class ArrayLists {

    public static void main(String[] args){

        ArrayList<String> friends = new ArrayList<>();

        friends.add("Jen");
        friends.add("Marielle");
        friends.add("Ian");
        friends.add("Mies");

        System.out.println("My friends are: "+ friends);

    }

}
