package com.zuitt.b193;

import java.util.HashMap;

public class HashMaps {

    public static void main(String[] args){

        HashMap<String, Integer>   inventory = new HashMap<>();

        inventory.put("Toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }

}
