package com.zuitt.b193;

import java.util.Scanner;

public class LeapYear {

    public static void main(String[] args){

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year.");
        int year = appScanner.nextInt();

        if (year % 4 == 0){

            if(year % 100 == 0){

                if(year % 400 == 0 )
                    System.out.println(year + " is a leap year.");
                else
                    System.out.println(year + " is NOT a leap year.");

            }else
                System.out.println(year + " is a leap year.");

        }else
            System.out.println(year + " is NOT a leap year.");
    }

}
